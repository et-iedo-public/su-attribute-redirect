###################
## CONFIGURATION ##
###################

ROOT_DIR := $(PWD)
COMMON := ${ROOT_DIR}/common
CODE_DIR := ${ROOT_DIR}/code
TOOLS := aws vault docker cw xmllint
TOOLS_NPM :=
FRAMEWORK_DIR := ${COMMON}/framework
FRAMEWORK_REPO := https://code.stanford.edu/et-iedo-public/cardinal-faas-framework.git

## BUILD DIR
ifndef BUILD_DIR
	BUILD_DIR := ${ROOT_DIR}/build
endif

## FRAMEWORK INSTALL
ifeq ($(MAKELEVEL),0)
	ifeq ($(wildcard ${FRAMEWORK_DIR}/.git/),)
		_ := $(shell >&2 echo)
		_ := $(shell echo Installing framework from GIT into ${FRAMEWORK_DIR}...)
		_ := $(shell mkdir -p ${FRAMEWORK_DIR})
		_ := $(shell git clone ${FRAMEWORK_REPO} ${FRAMEWORK_DIR})
	else
		_ := $(shell cd ${FRAMEWORK_DIR} && git pull)
	endif
endif

##############
## INCLUDES ##
##############

# NOTE: Not utilizing the 'frontend.mk' component since this application
# uses a CDN in front of a backend API function as the frontend.
include ${FRAMEWORK_DIR}/makefile_parts/framework.mk
include ${FRAMEWORK_DIR}/makefile_parts/resources.mk
include ${FRAMEWORK_DIR}/makefile_parts/backend.mk
include ${FRAMEWORK_DIR}/makefile_parts/aws-logs.mk
include ${FRAMEWORK_DIR}/makefile_parts/cognito-sp-config.mk

######################
## DOMAIN RESOURCES ##
######################

.PHONY: domain-init
domain-init: export RESOURCES_TYPE=domain
domain-init: resources-init ### Domain initialize code

.PHONY: domain-export
domain-export: export RESOURCES_TYPE=domain
domain-export: resources-export ### Domain export configuration

.PHONY: domain-deploy
domain-deploy: export RESOURCES_TYPE=domain
domain-deploy: resources-deploy ### Domain deployment

.PHONY: domain-remove
domain-remove: export RESOURCES_TYPE=domain
domain-remove: resources-remove ### Domain removal

.PHONY: domain-info
domain-info: export RESOURCES_TYPE=domain
domain-info: resources-info ### Domain information

###################
## DNS RESOURCES ##
###################

.PHONY: fqdn-init
fqdn-init: export RESOURCES_TYPE=fqdn
fqdn-init: resources-init ### Service FQDN initialize code

.PHONY: fqdn-export
fqdn-export: export RESOURCES_TYPE=fqdn
fqdn-export: resources-export ### Service FQDN export configuration

.PHONY: fqdn-deploy
fqdn-deploy: export RESOURCES_TYPE=fqdn
fqdn-deploy: resources-deploy ### Service FQDN deployment

.PHONY: fqdn-remove
fqdn-remove: export RESOURCES_TYPE=fqdn
fqdn-remove: resources-remove ### Service FQDN removal

.PHONY: fqdn-info
fqdn-info: export RESOURCES_TYPE=fqdn
fqdn-info: resources-info ### Service FQDN information

#################
## APPLICATION ##
#################

.PHONY: app-init
app-init: build #### Initialize the application stack
	@$(MAKE) domain-init
	@$(MAKE) domain-export
	@$(MAKE) resources-init
	@$(MAKE) resources-export
	@$(MAKE) fqdn-init
	@$(MAKE) fqdn-export
	@$(MAKE) backend-init
	@$(MAKE) backend-export

.PHONY: app-deploy
app-deploy: build #### Deploy the application stack (no fqdn)
	@$(MAKE) domain-init
	@$(MAKE) domain-export
	@$(MAKE) resources-init
	@$(MAKE) resources-deploy
	@$(MAKE) resources-export
	@$(MAKE) fqdn-init
	@$(MAKE) fqdn-deploy
	@$(MAKE) fqdn-export
	@$(MAKE) backend-init
	@$(MAKE) backend-deploy
	@$(MAKE) backend-export
	@$(MAKE) info

.PHONY: app-remove
app-remove: build #### Remove the application stack
	@$(MAKE) domain-init
	@$(MAKE) domain-export
	@$(MAKE) resources-init
	@$(MAKE) resources-export
	@$(MAKE) fqdn-init
	@$(MAKE) fqdn-export
	@$(MAKE) backend-init
	@$(MAKE) fqdn-remove
	@$(MAKE) backend-remove
	@$(MAKE) resources-remove

.PHONY: info
info: #### Show application information
	@$(MAKE) domain-info
	@$(MAKE) resources-info
	@$(MAKE) backend-info
	@printf "\e[0;33m\e[4mService URLs (Deploy DNS with 'make fqdn-deploy')\e[0m\nhttp://${DNS_NAME}/\nhttps://${DNS_NAME}/\n\n"

# EOF

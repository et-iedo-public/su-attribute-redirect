####
# IMPORT DOMAIN
####

data "aws_route53_zone" "main" {
  name = var.DNS_ZONE_NAME
}

####
# IMPORT CDN
####

data "local_file" "resources" {
  filename = "../resources/resources.json"
}
data "aws_cloudfront_distribution" "cdn" {
  id = jsondecode(data.local_file.resources.content).cdn.id
}

####
# CDN DNS
####

resource "aws_route53_record" "cdn" {
  name    = var.DNS_NAME
  type    = "A"
  zone_id = data.aws_route53_zone.main.zone_id
  alias {
    name                   = data.aws_cloudfront_distribution.cdn.domain_name
    zone_id                = data.aws_cloudfront_distribution.cdn.hosted_zone_id
    evaluate_target_health = false
  }
  allow_overwrite = true
}
resource "aws_route53_record" "cdn_ipv6" {
  name    = var.DNS_NAME
  type    = "AAAA"
  zone_id = data.aws_route53_zone.main.zone_id
  alias {
    name                   = data.aws_cloudfront_distribution.cdn.domain_name
    zone_id                = data.aws_cloudfront_distribution.cdn.hosted_zone_id
    evaluate_target_health = false
  }
  allow_overwrite = true
}

####
# RESOURCES OUTPUT
####

output "cdn" {
  value = {
    "fqdn" = aws_route53_record.cdn.fqdn
  }
}

provider "aws" {
  region = var.AWS_REGION
}

provider "aws" {
  alias  = "global"
  region = var.AWS_REGION_GLOBAL
}

terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = "2.70.0"
  }
  backend "s3" {}
}

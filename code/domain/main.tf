##########
# DOMAIN #
##########

resource "aws_route53_zone" "main" {
  name          = var.DNS_ZONE_NAME
  comment       = var.DNS_ZONE_NAME
  force_destroy = true
  tags          = var.TAGS
}

####
# RESOURCES OUTPUT
####

output "dns_zone" {
  value = {
    "name" = aws_route53_zone.main.name
    "id" = aws_route53_zone.main.id
    "name_servers" = aws_route53_zone.main.name_servers
  }
}

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "UIT ET IEDO - Stanford University <uit-et-eit-iedo-staff@office365stanford.onmicrosoft.com>"
__license__ = "Apache License 2.0"
"""
    redirect.py
    ~~~~~~~~
    Attribute value URL redirect
"""

import os, sys, json, time, urllib.parse, requests, boto3, logging
from http.cookies import SimpleCookie
from jose import jwt
from jose.utils import base64url_decode

## INIT COGNITO
cognito = boto3.client("cognito-idp")

## ENVIRONMENT VARIABLES
ATTRIBUTE_NAME = os.environ.get("ATTRIBUTE_NAME", "attribute")
ATTRIBUTE_URL_MAP_JSON = os.environ.get("ATTRIBUTE_URL_MAP_JSON", "{}")
ATTRIBUTE_UNASSIGNED_MESSAGE = os.environ.get("ATTRIBUTE_UNASSIGNED_MESSAGE", "")
USER_POOL_ID = os.environ.get("USER_POOL_ID", "")
USER_POOL_DOMAIN = os.environ.get("USER_POOL_DOMAIN", "")
USER_POOL_ENDPOINT = os.environ.get("USER_POOL_ENDPOINT", "")
USER_POOL_APP_CLIENT_ID = os.environ.get("USER_POOL_APP_CLIENT_ID", "")
USER_POOL_IDP_PROVIDER_NAME = os.environ.get("USER_POOL_IDP_PROVIDER_NAME", "idp")

## ATTRIBUTE UNASSIGNED RESPONSE
ATTRIBUTE_UNASSIGNED_RESPONSE_BODY = """<html>
<head><title>error</title></head>
<body><h1>ERROR!</h1>
<p>{0}</p>
</body>
</html>""".format(
    ATTRIBUTE_UNASSIGNED_MESSAGE
)

## CONFIGURE LOGGER
# The Lambda environment pre-configures a handler logging to stderr. If a handler is
# already configured, `.basicConfig` does not execute. Thus we set the level directly.
if len(logging.getLogger().handlers) > 0:
    logging.getLogger().setLevel(logging.INFO)
else:
    logging.basicConfig(
        stream=sys.stdout,
        level=os.environ.get("LOGLEVEL", "INFO"),
        format="%(levelname)-8s %(message)s",
    )
    logging.getLogger(__name__)


## RESPONSE REDIRECT
def responseRedirect(redirect_url):
    return {
        "statusCode": "302",
        "headers": {"Location": redirect_url,},
    }


## RESPONSE DENY
def responseDeny():
    return {
        "statusCode": "401",
        "body": "Unauthorized",
        "headers": {"Content-Type": "text/html",},
    }


## RESPONSE DENY
def responseError(error):
    return {
        "statusCode": "500",
        "body": error,
        "headers": {"Content-Type": "text/html",},
    }


## RESPONSE UNASSIGNED
def responseUnassigned():
    return {
        "statusCode": "200",
        "body": ATTRIBUTE_UNASSIGNED_RESPONSE_BODY,
        "headers": {"Content-Type": "text/html",},
    }


## RESPONSE COOKIES
def responseCookies(redirect_url, set_cookies):
    return {
        "statusCode": "302",
        "headers": {"Location": redirect_url,},
        "multiValueHeaders": {"Set-Cookie": set_cookies,},
    }


## REQUEST COOKIE
def requestCookie(event, name):
    cookie = ""
    cookieHeader = []
    if event.get("multiValueHeaders"):
        cookieHeader = event["multiValueHeaders"].get(
            "Cookie", event["multiValueHeaders"].get("cookie", [])
        )
    if cookieHeader:
        cookies = {}
        sc = SimpleCookie()
        for c in cookieHeader:
            sc.load(c)
            for key, morsel in sc.items():
                cookies[key] = morsel.value
        cookie = cookies.get(name, "")
    return cookie


## GET AUTH CODE
def getAuthCode(callback_url):
    url = "https://{0}/oauth2/authorize".format(USER_POOL_DOMAIN)
    params = urllib.parse.urlencode(
        {
            "scope": "openid",
            "response_type": "CODE",
            "client_id": USER_POOL_APP_CLIENT_ID,
            "redirect_uri": callback_url,
            "identity_provider": USER_POOL_IDP_PROVIDER_NAME,
        }
    )
    redirect_url = "{0}?{1}".format(url, params)
    return responseRedirect(redirect_url)


## GET TOKEN CERTIFICATES
def getTokenCertificates():
    certs = {"keys": []}
    url = "https://{0}/.well-known/jwks.json".format(USER_POOL_ENDPOINT)
    try:
        resp = requests.get(url)
        certs_resp = json.loads(resp.text)
        certs.update(certs_resp)
    except Exception as e:
        certs.update({"error": str(e)})
    return certs


## EXCHANGE AN AUTHORIZATION CODE FOR TOKENS
def codeExchange(code, callback_url):
    tokens = {"access_token": "", "id_token": ""}
    # Get client secret
    resp = cognito.describe_user_pool_client(
        UserPoolId=USER_POOL_ID, ClientId=USER_POOL_APP_CLIENT_ID,
    )
    client_secret = None
    if resp.get("UserPoolClient"):
        client_secret = resp["UserPoolClient"].get("ClientSecret", None)
    # Get tokens
    url = "https://{0}/oauth2/token".format(USER_POOL_DOMAIN)
    params = {
        "grant_type": "authorization_code",
        "code_verifier": "CODE_VERIFIER",
        "client_id": USER_POOL_APP_CLIENT_ID,
        "redirect_uri": callback_url,
        "code": code,
    }
    try:
        resp = requests.post(
            url, auth=(USER_POOL_APP_CLIENT_ID, client_secret), data=params
        )
        tokens_resp = json.loads(resp.text)
        tokens.update(tokens_resp)
    except Exception as e:
        tokens.update({"error": str(e)})
    return tokens


## VERIFY ACCESS AND RETURN TOKEN DATA
def verifyAccess(access_token, certs):
    verify_options = {
        "verify_signature": True,
        "verify_aud": True,
        "verify_iat": True,
        "verify_exp": True,
        "verify_nbf": True,
        "verify_iss": True,
        "verify_sub": True,
        "verify_jti": True,
        "verify_at_hash": True,
        "leeway": 0,
    }
    try:
        # Verify token signature and validate reserved claims
        access_token_data = jwt.decode(
            access_token,
            certs,
            issuer="https://{0}".format(USER_POOL_ENDPOINT),
            options=verify_options,
        )
        access = True
    except:
        access_token_data = {}
        access = False
    return access, access_token_data


## VERIFY IDENTITY AND RETURN ID TOKEN DATA
def verifyIdentity(id_token, access_token, certs):
    verify_options = {
        "verify_signature": True,
        "verify_aud": True,
        "verify_iat": True,
        "verify_exp": True,
        "verify_nbf": True,
        "verify_iss": True,
        "verify_sub": True,
        "verify_jti": True,
        "verify_at_hash": True,
        "leeway": 0,
    }
    try:
        # Verify token signature and validate reserved claims
        id_token_data = jwt.decode(
            id_token,
            certs,
            audience=USER_POOL_APP_CLIENT_ID,
            access_token=access_token,
            options=verify_options,
        )
        identity = True
    except:
        id_token_data = {}
        identity = False
    return identity, id_token_data


## SET ATTRIBUTE COOKIES
def setAttributeCookies(id_token_data):
    try:
        set_cookies = []
        attribute_value = id_token_data.get(
            "custom:{0}".format(ATTRIBUTE_NAME), "unassigned"
        )
        exp = time.strftime(
            "%a, %d %b %Y %H:%M:%S GMT", time.gmtime(id_token_data["exp"])
        )
        cookie = "{0}={1}; expires={2}; path=/; SameSite=Lax; Secure; HttpOnly".format(
            ATTRIBUTE_NAME, attribute_value, exp
        )
        set_cookies.append(cookie)
    except:
        set_cookies = []
    return set_cookies


## AUTHORIZE
def authorize(event, code):
    redirect_url = "https://{0}{1}".format(
        event["headers"]["Host"], event["requestContext"]["path"]
    )
    # Get a code
    if not code:
        return getAuthCode(redirect_url)
    # Get token certs
    certs = getTokenCertificates()
    # Exchange code for tokens
    tokens = codeExchange(code, redirect_url)
    # Verify access and get access token data
    access, access_token_data = verifyAccess(tokens["access_token"], certs)
    if access == False:
        return responseDeny()
    # Verify identity and get id token data
    identity, id_token_data = verifyIdentity(
        tokens["id_token"], tokens["access_token"], certs
    )
    if identity == False:
        return responseDeny()
    # Set cookies and respond
    set_cookies = setAttributeCookies(id_token_data)
    return responseCookies(redirect_url, set_cookies)


## HANDLER
def handler(event, context):
    # Check for code parameter
    code = None
    if event.get("queryStringParameters"):
        code = event["queryStringParameters"].get("code")
        error = event["queryStringParameters"].get("error_description")
        if error:
            logging.error("Server Error: {0}".format(error))
            return responseError(error)
    if code:
        return authorize(event, code)
    # Check for cookie
    cookie_attribute = requestCookie(event, ATTRIBUTE_NAME)
    # Cookie found: redirect to set location
    if cookie_attribute:
        try:
            ATTRIBUTE_URL_MAP = json.loads(ATTRIBUTE_URL_MAP_JSON)
        except:
            ATTRIBUTE_URL_MAP = {}
        REDIRECT_URL = ATTRIBUTE_URL_MAP.get(cookie_attribute)
        if REDIRECT_URL:
            return responseRedirect(REDIRECT_URL)
        else:
            return responseUnassigned()
    # Cookie missing: authorize and set cookie
    return authorize(event, code)

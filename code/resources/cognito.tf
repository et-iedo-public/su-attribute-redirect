###########
# COGNITO #
###########

####
# USER POOL
####

resource "aws_cognito_user_pool" "user_pool" {
  name = "${var.APP_NAME}-${var.STAGE}"
  schema {
    # This is a standard OpenID attribute
    name                = "name"
    attribute_data_type = "String"
    mutable             = true
    required            = true
  }
  schema {
    # This is a standard OpenID attribute
    name                = "email"
    attribute_data_type = "String"
    mutable             = true
    required            = true
  }
  schema {
    # This is a standard OpenID attribute
    name                = "preferred_username"
    attribute_data_type = "String"
    mutable             = true
    required            = true
  }
  schema {
    # This is a custom OpenID attribute.
    name                = var.ATTRIBUTE_NAME
    attribute_data_type = "String"
    mutable             = true
    required            = false
    string_attribute_constraints {
      min_length = "0"
      max_length = "2048"
    }
  }
  mfa_configuration        = "OFF"
  auto_verified_attributes = []
  admin_create_user_config {
    allow_admin_create_user_only = true
  }
  user_pool_add_ons {
    advanced_security_mode = "OFF"
  }
  lifecycle {
    ignore_changes = [ schema ]
  }
  tags = var.TAGS
}

# Setting Cognito RecoveryMechanisms is currently not supported by Terraform.
# This needs to be set to admin_only for better security.
resource "null_resource" "setup_account_recovery_settings" {
  depends_on = [ aws_cognito_user_pool.user_pool ]
  triggers = { user_pool_id = aws_cognito_user_pool.user_pool.id }
  provisioner "local-exec" {
    command = "aws cognito-idp update-user-pool --user-pool-id ${aws_cognito_user_pool.user_pool.id} --account-recovery-setting 'RecoveryMechanisms=[{Priority=1,Name=admin_only}]' --admin-create-user-config 'AllowAdminCreateUserOnly=true' --user-pool-add-ons 'AdvancedSecurityMode=OFF' --region ${var.AWS_REGION}"
  }
}

####
# USER POOL IDP
####

resource "aws_cognito_identity_provider" "user_pool" {
  depends_on    = [ aws_cognito_user_pool.user_pool ]
  provider_name = var.SAML_IDP_NAME
  provider_type = "SAML"
  user_pool_id  = aws_cognito_user_pool.user_pool.id
  provider_details = {
    MetadataURL = var.SAML_IDP_METADATA_URL
    IDPSignout  = false
    # Set to ignore and ignored below. This gets automatically
    # updated by the Metadata.xml - ignore_changes to prevent
    # terraform trying to update with each run. See:
    # https://github.com/terraform-providers/terraform-provider-aws/issues/4807#issuecomment-599725754
    SSORedirectBindingURI = "ignored"
  }
  attribute_mapping = var.SAML_IDP_ATTRIBUTE_MAP
  lifecycle {
    ignore_changes = [
      provider_details["SSORedirectBindingURI"]
    ]
  }
}

####
# USER POOL DOMAIN
####

resource "aws_cognito_user_pool_domain" "user_pool" {
  depends_on   = [ aws_cognito_user_pool.user_pool ]
  domain       = "${var.APP_NAME}-${var.STAGE}"
  user_pool_id = aws_cognito_user_pool.user_pool.id
}

####
# USER POOL CLIENT
####

resource "aws_cognito_user_pool_client" "user_pool" {
  depends_on      = [ aws_cognito_user_pool.user_pool ]
  name            = "${var.APP_NAME}-${var.STAGE}"
  user_pool_id    = aws_cognito_user_pool.user_pool.id
  generate_secret = true
  explicit_auth_flows = [
    "ALLOW_CUSTOM_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH"
  ]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows    = [ "code" ]
  allowed_oauth_scopes   = [ "openid" ]
  refresh_token_validity = "30"
  prevent_user_existence_errors = "LEGACY"
  supported_identity_providers = [
    aws_cognito_identity_provider.user_pool.provider_name
  ]
  callback_urls = [ "https://${var.DNS_NAME}/" ]
}

####
# RESOURCES OUTPUT
####

output "cognito" {
  value = {
    "fqdn" = "${aws_cognito_user_pool_domain.user_pool.domain}.auth.${var.AWS_REGION}.amazoncognito.com"
    "user_pool_id"  = aws_cognito_user_pool.user_pool.id
    "user_pool_arn" = aws_cognito_user_pool.user_pool.arn
    "user_pool_endpoint" = aws_cognito_user_pool.user_pool.endpoint
    "user_pool_domain_id" = aws_cognito_user_pool_domain.user_pool.id
    "user_pool_idp_provider_name" = aws_cognito_identity_provider.user_pool.provider_name
    "user_pool_app_client_id" = aws_cognito_user_pool_client.user_pool.id
  }
}

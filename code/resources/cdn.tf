#######
# CDN #
#######

resource "aws_cloudfront_distribution" "cdn" {
  depends_on          = [ aws_acm_certificate_validation.cdn_cert, aws_acm_certificate_validation.lambda_cert ]
  comment             = "${var.APP_NAME}-${var.STAGE}"
  aliases             = [ aws_acm_certificate.cdn_cert.domain_name ]
  enabled             = true
  is_ipv6_enabled     = true
  wait_for_deployment = false
  price_class         = "PriceClass_100"
  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn            = aws_acm_certificate.cdn_cert.arn
    minimum_protocol_version       = "TLSv1.2_2018"
    ssl_support_method             = "sni-only"
  }
  origin {
    origin_id   = "default"
    domain_name = aws_acm_certificate.lambda_cert.domain_name
    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 5
      origin_protocol_policy   = "https-only"
      origin_read_timeout      = 30
      origin_ssl_protocols     = [ "TLSv1.2" ]
    }
  }
  default_cache_behavior {
    target_origin_id       = "default"
    allowed_methods        = [ "GET", "HEAD" ]
    cached_methods         = [ "GET", "HEAD" ]
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = false
    viewer_protocol_policy = "redirect-to-https"
    forwarded_values {
      headers = [
        "Host",
        "Origin",
        "Referer",
      ]
      query_string = true
      cookies {
        forward = "all"
      }
    }
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = var.TAGS
}

####
# RESOURCES OUTPUT
####

output "cdn" {
  value = {
    "id" = aws_cloudfront_distribution.cdn.id
    "domain_name" = aws_cloudfront_distribution.cdn.domain_name
  }
}

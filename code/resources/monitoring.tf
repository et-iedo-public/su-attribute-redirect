##############
# MONITORING #
##############

####
# CLOUDWATCH DASHBOARD
####

resource "aws_cloudwatch_dashboard" "main" {
  dashboard_name = "${var.APP_NAME}-${var.STAGE}"
  dashboard_body = jsonencode({
    widgets = [
      {
        height     = 6
        properties = {
          metrics = [
            [
              "AWS/ApiGateway",
              "IntegrationLatency",
              "ApiName",
              "${var.APP_NAME}-${var.STAGE}",
              {
                color = "#9467bd"
                label = "Latency"
              },
            ],
          ]
          period  = 300
          region  = var.AWS_REGION
          stacked = true
          stat    = "Average"
          title   = "Average Latency"
          view    = "timeSeries"
        }
        type       = "metric"
        width      = 12
        x          = 12
        y          = 0
      },
      {
        height     = 6
        properties = {
          metrics = [
            [
              "AWS/Lambda",
              "Duration",
              "FunctionName",
              "${var.APP_NAME}-${var.STAGE}-main",
              {
                id    = "m1"
                label = "Maximum Lambda Duration"
              },
            ],
          ]
          period  = 300
          region  = var.AWS_REGION
          stacked = true
          stat    = "Maximum"
          title   = "Maximum Lambda Duration"
          view    = "timeSeries"
          yAxis   = {
            left  = {
              label     = ""
              showUnits = true
            }
            right = {
              showUnits = true
            }
          }
        }
        type       = "metric"
        width      = 12
        x          = 12
        y          = 6
      },
      {
        height     = 6
        properties = {
          metrics = [
            [
              {
                color      = "#d62728"
                expression = "FILL([m1,m2],0)"
                id         = "e1"
                label      = ""
                region     = var.AWS_REGION
              },
            ],
            [
              {
                color      = "#9467bd"
                expression = "FILL(m3,0) - FILL(m4,0)"
                id         = "e2"
                label      = "Cognito Federation Failures"
                region     = var.AWS_REGION
              },
            ],
            [
              "AWS/Lambda",
              "Errors",
              "FunctionName",
              "${var.APP_NAME}-${var.STAGE}-main",
              {
                color   = "#ff7f0e"
                id      = "m1"
                label   = "Lambda Errors"
                visible = false
              },
            ],
            [
              "AWS/ApiGateway",
              "5XXError",
              "ApiName",
              "${var.APP_NAME}-${var.STAGE}",
              {
                color   = "#1f77b4"
                id      = "m2"
                label   = "ApiGateway 5XX Errors"
                visible = false
              },
            ],
            [
              "AWS/Cognito",
              "FederationSuccesses",
              "UserPool",
              aws_cognito_user_pool.user_pool.id,
              "IdentityProvider",
              aws_cognito_identity_provider.user_pool.provider_name,
              "UserPoolClient",
              aws_cognito_user_pool_client.user_pool.id,
              {
                id      = "m3"
                stat    = "SampleCount"
                visible = false
              },
            ],
            [
              "...",
              {
                id      = "m4"
                visible = false
              },
            ],
          ]
          period  = 60
          region  = var.AWS_REGION
          stacked = true
          stat    = "Sum"
          title   = "Errors"
          view    = "timeSeries"
          yAxis   = {
            left  = {
              label     = "Error Count"
              showUnits = false
            }
            right = {
              showUnits = false
            }
          }
        }
        type       = "metric"
        width      = 12
        x          = 0
        y          = 6
      },
      {
        height     = 6
        properties = {
          metrics = [
            [
              {
                color      = "#2ca02c"
                expression = "FILL(m1,0)/PERIOD(m1)"
                id         = "e1"
                label      = "Traffic"
                region     = var.AWS_REGION
              },
            ],
            [
              "AWS/ApiGateway",
              "Count",
              "ApiName",
              "${var.APP_NAME}-${var.STAGE}",
              {
                id      = "m1"
                label   = "Count"
                period  = 60
                stat    = "Sum"
                visible = false
              },
            ],
          ]
          period  = 300
          region  = var.AWS_REGION
          stacked = true
          stat    = "Average"
          title   = "Traffic"
          view    = "timeSeries"
          yAxis   = {
            left  = {
              label     = "Requests Per Second"
              showUnits = false
            }
            right = {
              showUnits = false
            }
          }
        }
        type       = "metric"
        width      = 12
        x          = 0
        y          = 0
      },
      {
        height     = 6
        properties = {
          query   = "SOURCE '/aws/lambda/${var.APP_NAME}-${var.STAGE}-main' | fields @timestamp, @message | sort @timestamp desc | limit 1000"
          region  = var.AWS_REGION
          stacked = false
          title   = "Lambda Logs"
          view    = "table"
        }
        type       = "log"
        width      = 24
        x          = 0
        y          = 12
      },
      {
        height     = 6
        properties = {
          query   = "SOURCE '/aws/api-gateway/${var.APP_NAME}-${var.STAGE}' | fields @timestamp, @message | sort @timestamp desc | limit 1000"
          region  = var.AWS_REGION
          stacked = false
          title   = "API Gateway Logs"
          view    = "table"
        }
        type       = "log"
        width      = 24
        x          = 0
        y          = 18
      },
    ]
  })
}

################
# CERTIFICATES #
################

####
# CREATE AND VALIDATE CDN PUBLIC CERTIFICATE
####

resource "aws_acm_certificate" "cdn_cert" {
  provider                  = aws.global
  domain_name               = var.DNS_NAME
  validation_method         = "DNS"
  subject_alternative_names = []
  lifecycle {
    create_before_destroy = true
  }
  tags = merge(var.TAGS, { Name = "${var.APP_NAME}-${var.STAGE}" })
}
resource "aws_route53_record" "cdn_cert_validation" {
  depends_on      = [ aws_acm_certificate.cdn_cert ]
  name            = aws_acm_certificate.cdn_cert.domain_validation_options.0.resource_record_name
  type            = aws_acm_certificate.cdn_cert.domain_validation_options.0.resource_record_type
  zone_id         = data.aws_route53_zone.main.zone_id
  records         = [ aws_acm_certificate.cdn_cert.domain_validation_options.0.resource_record_value ]
  ttl             = 60
  allow_overwrite = true
}
resource "aws_acm_certificate_validation" "cdn_cert" {
  depends_on              = [ aws_route53_record.cdn_cert_validation ]
  provider                = aws.global
  certificate_arn         = aws_acm_certificate.cdn_cert.arn
  validation_record_fqdns = [ aws_route53_record.cdn_cert_validation.fqdn ]
}

####
# CREATE AND VALIDATE LAMBDA PUBLIC CERTIFICATE
####

resource "aws_acm_certificate" "lambda_cert" {
  depends_on                = [ aws_acm_certificate_validation.cdn_cert ]
  domain_name               = "api.${var.DNS_NAME}"
  validation_method         = "DNS"
  subject_alternative_names = [
    var.DNS_NAME,
  ]
  lifecycle {
    create_before_destroy = true
  }
  tags = merge(var.TAGS, { Name = "${var.APP_NAME}-${var.STAGE}" })
}
resource "aws_route53_record" "lambda_cert_validation" {
  depends_on      = [ aws_acm_certificate.lambda_cert ]
  name            = aws_acm_certificate.lambda_cert.domain_validation_options.0.resource_record_name
  type            = aws_acm_certificate.lambda_cert.domain_validation_options.0.resource_record_type
  zone_id         = data.aws_route53_zone.main.zone_id
  records         = [ aws_acm_certificate.lambda_cert.domain_validation_options.0.resource_record_value ]
  ttl             = 60
  allow_overwrite = true
}
resource "aws_acm_certificate_validation" "lambda_cert" {
  depends_on              = [ aws_route53_record.lambda_cert_validation ]
  certificate_arn         = aws_acm_certificate.lambda_cert.arn
  validation_record_fqdns = [
    aws_route53_record.lambda_cert_validation.fqdn,
    aws_route53_record.cdn_cert_validation.fqdn,
  ]
}

#############################
# API GATEWAY CUSTOM DOMAIN #
#############################

# We are configuring these in advance. They are used to provide static endpoint names
# so that they can be referenced by the Cognito resource (non-custom domains have a
# randomized name and can't be referenced by Cognito until after lamdba deployment).
# Also, SLS used by the backendf doesn't support using a pre-configured API Gateway
# at this time.

resource "aws_api_gateway_domain_name" "lambda_main" {
  depends_on      = [ aws_acm_certificate_validation.lambda_cert ]
  domain_name     = "api.${var.DNS_NAME}"
  security_policy = "TLS_1_2"
  regional_certificate_arn = aws_acm_certificate.lambda_cert.arn
  endpoint_configuration {
    types = [ "REGIONAL" ]
  }
  tags = var.TAGS
}
resource "aws_api_gateway_domain_name" "lambda_alt1" {
  depends_on      = [ aws_acm_certificate_validation.lambda_cert ]
  domain_name     = var.DNS_NAME
  security_policy = "TLS_1_2"
  regional_certificate_arn = aws_acm_certificate.lambda_cert.arn
  endpoint_configuration {
    types = [ "REGIONAL" ]
  }
  tags = var.TAGS
}

####
# API GATEWAY MAIN DNS
####

resource "aws_route53_record" "lambda" {
  depends_on = [ aws_api_gateway_domain_name.lambda_main ]
  name       = aws_acm_certificate.lambda_cert.domain_name
  type       = "A"
  zone_id    = data.aws_route53_zone.main.zone_id
  alias {
    name                   = aws_api_gateway_domain_name.lambda_main.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.lambda_main.regional_zone_id
    evaluate_target_health = false
  }
  allow_overwrite = true
}
resource "aws_route53_record" "lambda_ipv6" {
  depends_on = [ aws_api_gateway_domain_name.lambda_main ]
  name       = aws_acm_certificate.lambda_cert.domain_name
  type       = "AAAA"
  zone_id    = data.aws_route53_zone.main.zone_id
  alias {
    name                   = aws_api_gateway_domain_name.lambda_main.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.lambda_main.regional_zone_id
    evaluate_target_health = false
  }
  allow_overwrite = true
}

####
# RESOURCES OUTPUT
####

output "api_gateway" {
  value = {
    "fqdn" = aws_route53_record.lambda.fqdn
  }
}

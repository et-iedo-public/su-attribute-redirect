#######
# IAM #
#######

####
# API Gateway CloudWatch Role
####

resource "aws_iam_role" "apigw_cw" {
  name = "${var.APP_NAME}-${var.STAGE}-ApiGatewayCloudWatchRole"
  tags = var.TAGS
  assume_role_policy = data.aws_iam_policy_document.apigw_cw_policy.json
}
data "aws_iam_policy_document" "apigw_cw_policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [ "apigateway.amazonaws.com" ]
    }
    actions = [ "sts:AssumeRole" ]
  }
}
resource "aws_iam_role_policy_attachment" "apigw_cw_policy" {
  role       = aws_iam_role.apigw_cw.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}

####
# RESOURCES OUTPUT
####

output "iam" {
  value = {
    "apigw_cw_role" = aws_iam_role.apigw_cw.arn
  }
}

##########
# DOMAIN #
##########

data "aws_route53_zone" "main" {
  name = var.DNS_ZONE_NAME
}

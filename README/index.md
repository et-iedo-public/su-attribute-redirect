# Attribute Redirect Service Guide

## STEPS:

* Create your `common/env.defaults` file
* Run: `make`
* Run: `make domain-deploy`. Use the output (nameservers) for your delegated domain.
* Wait the appropriate time for your delegated domain to propegate across the internet.
* Run: `make app-deploy`
* Run: `make cognito-sp-config` and use the metadata to configure your SP.

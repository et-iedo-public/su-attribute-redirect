# Attribute Redirect Service

## Overview

The Attribute Redirect Service is a serverless, cloud based service that uses the [Cardinal FaaS Framework](https://code.stanford.edu/et-iedo-public/cardinal-faas-framework). This service provides savings in infrastructure and operational costs and provides maximum resiliency and scalability. This service is relatively generic and is meant to be used for multiple specific purposes.

##### Architecture

![Service](/README/images/service.png)

##### AWS services:
- Cloudfront
- API Gateway with custom domain
- AWS Lambda
- Cognito User Pool with external federated SAML 2.0 identity provider.
- Route 53 DNS
- Amazon Certificate Manager (ACM) for SSL endpoints

##### Lambda code:
- This is a custom function that checks for a cookie that contains a specified SAML attribute-value pair.
- If the cookie exists, the user is forwarded to the appropriate URL based on your predefined URL map.
- If the cookie doesn’t exist, the user is redirected to Cognito/SAML auth to obtain the SAML attribute, write it to a cookie, and redirect to the appropriate URL.

## Documentation
For more information, see the [Attribute Redirect Service Guide](/README/index.md)
